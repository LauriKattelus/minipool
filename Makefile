CC = g++
OBJS = ./src/*.cpp
LINK_INCLUDES = -I./include/
LINKER_FLAGS = -lSDL2 -lSDL2_image
OBJ_NAME = minipool
all : $(OBJS)
	$(CC) $(OBJS) $(LINK_INCLUDES) $(LINKER_FLAGS) -o $(OBJ_NAME)
clean:
	$(RM) count *.o *~ $(OBJ_NAME)
