#include "init.h"

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

bool running = true;

bool init(){
    bool success = true;
    
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow ("Snake", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    return success;
}

bool close(){
    bool success = true;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return success;
}

void clear_screen(){
    SDL_SetRenderDrawColor(renderer, 0x00,0x00,0x00,0x00);
    SDL_RenderClear(renderer);
}

void render_present(){
    SDL_RenderPresent(renderer);
}
