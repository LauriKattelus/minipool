#include <iostream>
#include "init.h"

int main(){
    std::cout << "minipool" << std::endl;

    init();

    SDL_Event e;

    while(running){
        while(SDL_PollEvent(&e) != 0){
            if(e.type == SDL_QUIT) {
                running = false;
            }
        }

        clear_screen();
        render_present();
    }

    close();

    return 0;
}